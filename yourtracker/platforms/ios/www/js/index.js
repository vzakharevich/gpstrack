var app = {
    debugState: true,

    // Application Constructor
    initialize: function() {
        $(".debug").hide();
        this.statusGPS();
    },

    statusGPS: function() {
        geoService.getData;
        this.debug("its work");
    },

    debug: function(msg) {
      if (this.debugState) {
        $(".debug").show();
        $(".debug").append(msg + '<br>');
      }
    }
};

var geoService = {
  onSuccess: function(position) {
      alert('Latitude: '          + position.coords.latitude          + '\n' +
            'Longitude: '         + position.coords.longitude         + '\n' +
            'Altitude: '          + position.coords.altitude          + '\n' +
            'Accuracy: '          + position.coords.accuracy          + '\n' +
            'Altitude Accuracy: ' + position.coords.altitudeAccuracy  + '\n' +
            'Heading: '           + position.coords.heading           + '\n' +
            'Speed: '             + position.coords.speed             + '\n' +
            'Timestamp: '         + position.timestamp                + '\n');
  },

  onError: function(error) {
      alert('code: '    + error.code    + '\n' +
            'message: ' + error.message + '\n');
  },

  getData: function() {
    return navigator.geolocation.getCurrentPosition(this.onSuccess, this.onError);
  }
}

app.initialize();
