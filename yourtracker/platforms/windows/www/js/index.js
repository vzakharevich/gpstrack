﻿var app = {
    debugState: true,

    // Application Constructor
    initialize: function() {
        $(".debug").hide();
        $(".error").hide();
        $("#show-to-map").hide();
        this.statusGPS();
    },

    statusGPS: function() {
        this.checkSupportDevice();
        //geoService.checkEnable();
        geoService.getData();
    },

    checkSupportDevice: function() {
      if (!geoService.checkPermission()) {
        this.debug("Это устройство не поддерживает геолокацию");
      }
    },

    showPosition: function(position) {
      this.debug('Latitude: '          + position.coords.latitude          + '<br>' +
            'Longitude: '         + position.coords.longitude         + '<br>' +
            'Altitude: '          + position.coords.altitude          + '<br>' +
            'Accuracy: '          + position.coords.accuracy          + '<br>' +
            'Altitude Accuracy: ' + position.coords.altitudeAccuracy  + '<br>' +
            'Heading: '           + position.coords.heading           + '<br>' +
            'Speed: '             + position.coords.speed             + '<br>' +
            'Timestamp: '         + position.timestamp                + '<br>');
    },

    debug: function(msg) {
      if (this.debugState) {
        $(".debug").show();
        $(".debug").append(msg + '<br>');
      }
    },

    geoError: function(error) {
      $(".error").show();
      $(".error").append(error.message + '<br>');
      $(".gps-to-on").addClass("needle");
      $(".gps-ready").addClass("success");
    },

    showToMap: function() {
      $("#main-menu").hide();
      $("#show-to-map").show();
      geoService.getDataToMap();
    },

    renderMap: function() {
      var latitude  = position.coords.latitude;
      var longitude = position.coords.longitude;

      var img = new Image();
      img.src = "http://maps.googleapis.com/maps/api/staticmap?center=" + latitude + "," + longitude + "&zoom=13&size=300x300&sensor=true";
      $(".map-wrap").appendChild(img);
    },
};

var tInterface = {

}

var geoService = {
  getData: function() {
    navigator.geolocation.getCurrentPosition(app.showPosition, app.geoError);
  },

  checkPermission: function () {
    if (navigator.geolocation) {
      return true;
    } else {
      return false;
    }
  },

  getDataToMap: function () {
    navigator.geolocation.getCurrentPosition(app.renderMap, app.geoError);
  },
}

app.initialize();

$( ".shop-me-map" ).click(function() {
  app.showToMap();
});
