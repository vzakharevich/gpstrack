var app = {
    debugState: true,

    // Application Constructor
    initialize: function() {
        $(".debug").hide();
        $(".error").hide();
        $("#show-to-map").hide();
        geoService.checkEnable();
        this.statusGPS();
    },

    statusGPS: function() {
        this.checkSupportDevice();
    },

    checkSupportDevice: function() {
      if (!geoService.checkPermission()) {
        this.debug("Это устройство не поддерживает геолокацию");
      }
    },

    showPosition: function(position) {
      data = 'Latitude: '          + position.coords.latitude          + '<br>' +
            'Longitude: '         + position.coords.longitude         + '<br>' +
            'Altitude: '          + position.coords.altitude          + '<br>' +
            'Accuracy: '          + position.coords.accuracy          + '<br>' +
            'Altitude Accuracy: ' + position.coords.altitudeAccuracy  + '<br>' +
            'Heading: '           + position.coords.heading           + '<br>' +
            'Speed: '             + position.coords.speed             + '<br>' +
            'Timestamp: '         + position.timestamp                + '<br>';

      console.log(data);
      app.debug(data);
    },

    debug: function(msg) {
      if (app.debugState) {
        $(".debug").show();
        $(".debug").html(msg + '<br>');
      }
    },

    showMainMenu: function() {
      $("#show-to-map").hide();
      $("#main-menu").show();
    },

    showToMap: function() {
      $("#main-menu").hide();
      $("#show-to-map").show();
      app.debug("this app.showToMap");
      geoService.getDataToMap();
    },

    renderMap: function(position) {
      var latitude  = position.coords.latitude;
      var longitude = position.coords.longitude;
      console.log(latitude + "_" + longitude);
      var coords = new google.maps.LatLng(latitude, longitude);

        var options = {
          zoom: 8,
          center: coords,
          mapTypeControl: true,
          navigationControlOptions: {
          	style: google.maps.NavigationControlStyle.SMALL
          },
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("mapcontainer"), options);

        var marker = new google.maps.Marker({
            position: coords,
            map: map,
            title:"Ты тут!"
        });

    },
};

var tInterface = {
  checkEnable: function() {
    $(".gps-ready").addClass("success");
    $(".gps-to-on").hide();
  },

  geoError: function(error) {
    $(".error").show();
    $(".error").html(error.message);
    $(".gps-to-on").addClass("needle");
    $(".gps-ready").hide();
  },
}

var geoService = {
  getData: function() {
    navigator.geolocation.getCurrentPosition(app.showPosition, tInterface.geoError);
  },

  checkPermission: function () {
    if (navigator.geolocation) {
      return true;
    } else {
      return false;
    }
  },

  getDataToMap: function () {
    navigator.geolocation.getCurrentPosition(app.renderMap, tInterface.geoError);
  },

  checkEnable: function () {
    navigator.geolocation.getCurrentPosition(tInterface.checkEnable, tInterface.geoError);
  },
}

app.initialize();

$( ".show-me-data" ).click(function() {
  navigator.geolocation.getCurrentPosition(app.showPosition, tInterface.geoError);
});

$( ".show-me-map" ).click(function() {
  app.showToMap();
});

$( ".to-main-menu" ).click(function() {
  app.showMainMenu();
});

$( ".debug" ).click(function() {
  $( ".debug" ).hide();
});

$( ".error" ).click(function() {
  $( ".error" ).hide();
});
